package pt.jcampos.cartas.Menus;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import pt.jcampos.cartas.Menus.Menu;
import pt.jcampos.cartas.R;

public class MenuViewAdapter extends BaseAdapter {
    private static final String COLLECTION_PRODUTOS = "PRODUTOS";
    private static final String TAG = "MenuViewAdapter";
    private FirebaseFirestore db;
    private String finalPrice;

    private final List<Menu> items;

    private TextView txtMenuPreço;
    private String finalPriceOnTextView;

    public MenuViewAdapter(final List<Menu> items) { this.items = items; }

    public int getCount() {
        return this.items.size();
    }

    public Object getItem(int arg0) { return this.items.get(arg0); }

    public long getItemId(int arg0) {
        return arg0;
    }

    public View getView(int arg0, View arg1, ViewGroup arg2) {
        // TODO Auto-generated method stub
        final Menu row = this.items.get(arg0);
        View itemView = null;

        if (arg1 == null) {
            LayoutInflater inflater = (LayoutInflater) arg2.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.menu_item, null);
        } else {
            itemView = arg1;
        }

        // Set the text of the row
        TextView txtMenuID = (TextView) itemView.findViewById(R.id.txtMenuName);
        txtMenuID.setText(row.getId());

        final ArrayList<String> produtos = row.getProdutos();

        TextView txtProdutoPrato = (TextView) itemView.findViewById(R.id.txtProdutoPrato);
        txtProdutoPrato.setText(produtos.get(0));

        TextView txtProdutoBebida = (TextView) itemView.findViewById(R.id.txtProdutoBebida);
        txtProdutoBebida.setText(produtos.get(1));

        TextView txtProdutoSobremesa = (TextView) itemView.findViewById(R.id.txtProdutoSobremesa);
        txtProdutoSobremesa.setText(produtos.get(2));

        //String finalPrice = CalculateTotalOf(produtos, itemView);
        //txtMenuPreço.setText(finalPrice);
        txtMenuPreço = (TextView) itemView.findViewById(R.id.txtMenuPreço);
        txtMenuPreço.setText(row.getPreço());
        txtMenuPreço.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView txt = (TextView)v;
                txt.setText(CalculateTotalOf_1(produtos));
            }
        });

        return itemView;
    }

    private String CalculateTotalOf(ArrayList<String> produtos, final View itemView) {
        db = FirebaseFirestore.getInstance();

        CollectionReference produtosCR = db.collection(COLLECTION_PRODUTOS);

        // foreach item of the arraylist
        for (String prd : produtos) { /*query*/ }
        Query query = produtosCR.whereEqualTo("Nome", produtos.get(0));

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            String price;
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        finalPrice = document.get("Preço").toString();

                        txtMenuPreço = (TextView) itemView.findViewById(R.id.txtMenuPreço);
                        txtMenuPreço.setText(finalPrice);

                    }

                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
            }
        });

        //Log.d(TAG, "finalPrice: " + finalPrice);
        return finalPrice;
    }

    private String CalculateTotalOf_1(ArrayList<String> produtos) {
        db = FirebaseFirestore.getInstance();

        CollectionReference produtosCR = db.collection(COLLECTION_PRODUTOS);

        // foreach item of the arraylist
        //for (String prd : produtos) { /*query*/ }
        Query query = produtosCR.whereEqualTo("Nome", produtos.get(0));

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            String price;
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        finalPrice = document.get("Preço").toString();
                    }

                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
            }
        });

        Log.d(TAG, "finalPrice: " + produtos.get(0)+ " => " + finalPrice);
        return finalPrice;
    }

    public void setFinalPriceOnTextView(String finalPriceOnTextView) {
        this.finalPriceOnTextView = finalPriceOnTextView;
        txtMenuPreço.setText(finalPriceOnTextView);
    }
}
