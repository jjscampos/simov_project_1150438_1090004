package pt.jcampos.cartas.Menus;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    //Pager fragment array list, fillded inside of pager item
    private final List<Fragment> mFragmentList = new ArrayList<>();

    //Pager title array list
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }


    //adding fragments and title method
    public void addFrag(Fragment fragment, String title) {

//        ListViewFragment lv =  (ListViewFragment) fragment;
//        lv.adapter.setItemChecked(new ListViewAdapter.ItemChecked() {
//            @Override
//            public void onItemChecked(String name) {
//                getSelectedItemsCount();
//            }
//        });

        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    private int getSelectedItemsCount(){
        int counter = 0;
        for (int i = 0; i < mFragmentList.size(); i++) {
            ListViewFragment lv =  (ListViewFragment)getItem(i);
            if (lv.getSelectedItem() != "") counter++;
        }
        return counter;
    }
}
