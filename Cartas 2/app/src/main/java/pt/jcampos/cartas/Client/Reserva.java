package pt.jcampos.cartas.Client;

public class Reserva {
    private String id, dataHora, Estado, Email, numPessoas, MenuId, nomeCliente;

    public Reserva (String dataHora, String Estado, String Email, String numPessoas, String MenuId, String nomeCliente)
    {
        this.dataHora = dataHora;
        this.Estado = Estado;
        this.Email = Email;
        this.numPessoas = numPessoas;
        this.MenuId = MenuId;
        this.nomeCliente = nomeCliente;
    }

    public String getDataHora() {
        return dataHora;
    }

    public String getEstado() {
        return Estado;
    }

    public String getEmail() { return Email; }

    public String getNumPessoas() {
        return numPessoas;
    }

    public String getMenuId() {
        return MenuId;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setEstado(String estado) {
        this.Estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
