package pt.jcampos.cartas.Produtos;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import pt.jcampos.cartas.Database.DatabaseHelper;
import pt.jcampos.cartas.R;
import pt.jcampos.cartas.Utils.Global;

public class ProductAdd extends Activity {
    private static final String TAG = "PRODUTCT_ADD::::...";

    private final int PICK_IMAGE_REQUEST = 71, RES_CODE_TEST = 2;

    private Button btnSave;
    private RadioGroup rdgTipo;
    private EditText edtNome, edtDesc, edtPreço;
    private ImageView imgvProduct;

    private String tipo = "", productId = "", imageURI = "";

    private DatabaseHelper dbh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_add);

        dbh = new DatabaseHelper(this);

        rdgTipo = (RadioGroup) findViewById(R.id.rgTipo);
        rdgTipo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                tipo = rb.getText().toString();
                btnSave.setEnabled(true);
            }
        });

        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateSQLiteDB();

                // put the String to pass back into an Intent and close this activity
                Intent newIntent = new Intent();
                newIntent.putExtra("NewProduct", "");
                setResult(RESULT_OK, newIntent); // by definition Activity.RESULT_OK = 0
                finish();
            }
        });

        imgvProduct = (ImageView) findViewById(R.id.imgvProduct);
        imgvProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

        edtNome = (EditText) findViewById(R.id.edtNome);
        edtDesc = (EditText) findViewById(R.id.edtDesc);
        edtPreço = (EditText) findViewById(R.id.edtPreço);

        // Verifica se estamos a editar um produto
        productId = getIntent().getStringExtra("ProductID");

        Log.d(TAG, "onCreate::productId: "+ productId +"\n");

        if (!"".equals(productId)) {
            edtNome.setEnabled(false);
            loadProduto();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume::productId: "+ productId +"\n");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop::productId: "+ productId +"\n");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //if the result is from the PICK_IMAGE_REQUEST
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null )
        {
            Uri filePath = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imgvProduct.setImageBitmap(bitmap);

                saveImageToInternalStorage(bitmap);
            }
            catch (IOException e)
            {
                ShowToast(e.getMessage());
            }
        }
    }

    private void loadProduto() {
        Log.d(TAG, "Start loadProduto\n");

        String query = "SELECT p.* "
                + "FROM PRODUTO p "
                + "WHERE p.Nome = '" + productId + "' "
                + "ORDER By p.Nome ";

        SQLiteDatabase db = dbh.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String produtoId = cursor.getString(cursor.getColumnIndex("Nome"));
                String preço = cursor.getString(cursor.getColumnIndex("Preço"));
                String Desc = cursor.getString(cursor.getColumnIndex("Desc"));
                String imagem = cursor.getString(cursor.getColumnIndex("Imagem"));
                String tipo = cursor.getString(cursor.getColumnIndex("Tipo"));

                imageURI = imagem;
                Log.d(TAG, "imageURI: "+ imageURI +"\n");

                edtNome.setText(produtoId);
                edtDesc.setText(Desc);
                edtPreço.setText(preço);

                if (tipo.equals("Prato")) rdgTipo.check(R.id.rbTipoPrato);
                if (tipo.equals("Bebida")) rdgTipo.check(R.id.rbTipoBebida);
                if (tipo.equals("Sobremesa")) rdgTipo.check(R.id.rbTipoSobremesa);

            } while (cursor.moveToNext());
            if (!"".equals(imageURI)) loadImage(imageURI);

            Log.d(TAG, "finishing loadProduto\n");
        }
    }

    private void loadImage(String imageURI) {
        //Glide.with(this).using(new FirebaseImageLoader()).load(imageURI).into(imgvProduct);
        Glide.with(this).load(imageURI).into(imgvProduct);
    }

    /*

     */
    private void updateSQLiteDB() {
        if ("".equals(productId)) {
            dbh.insertProduto(edtNome.getText().toString(), edtDesc.getText().toString(),
                    tipo,edtPreço.getText().toString(), imageURI,true );
        }else {
            dbh.updateProduto(edtNome.getText().toString(), edtDesc.getText().toString(),
                    tipo,edtPreço.getText().toString(), imageURI,true );
        }
    }

    /*

     */
    private void saveImageToInternalStorage(Bitmap bitmap) {
        String appDataDir = getBaseContext().getApplicationInfo().dataDir;
        String imagesFolder = appDataDir + Global.TEMP_IMAGE_DIR ;
        File folder = new File(imagesFolder);
        folder.mkdir();

        String imageFullPath = imagesFolder + productId + ".png";

        try {
            FileOutputStream fos = new FileOutputStream(imageFullPath);
            bitmap.compress(Bitmap.CompressFormat.PNG, Global.EXPORT_IMAGE_QUALITY, fos);
            fos.flush();
            fos.close();
            imageURI = imageFullPath;

            Log.d(TAG, "saveImageToInternalStorage: "+ imageFullPath +"\n");

        } catch (Exception e) {
            ShowToast(e.getMessage());
        }
    }

    /*
     *
     * */
    private void ShowToast(String message) {
        Toast.makeText(ProductAdd.this, message, Toast.LENGTH_SHORT).show();
    }

}
