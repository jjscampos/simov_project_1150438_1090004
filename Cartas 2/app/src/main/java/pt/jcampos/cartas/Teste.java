package pt.jcampos.cartas;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import pt.jcampos.cartas.Database.DatabaseHelper;
import pt.jcampos.cartas.Menus.Menu;
import pt.jcampos.cartas.R;

public class Teste extends Activity {
    private static final String COLLECTION_PRODUTOS = "PRODUTOS";
    private static final String COLLECTION_MENUS = "MENUS";

    private static final String TAG = "TESTE:::... ";
    private ArrayList<Menu> menus;

    private FirebaseFirestore db;
    private DatabaseHelper db_h;

    private EditText edtMenuNome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teste);

        db = FirebaseFirestore.getInstance();

        db_h = new DatabaseHelper(this);

        Button btnLoad = (Button) findViewById(R.id.btnLoad);
        btnLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadProdutos();
                loadMenus();
            }
        });

        Button btnPrint = (Button) findViewById(R.id.btnPrint);
        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printTableProduto();
                printTableMenu();
                printTableMenuProduto();
            }
        });

        Button btnGetPreços = (Button) findViewById(R.id.btnGetPreços);
        btnGetPreços.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPreços();
            }
        });

        Button btnInsertSQLite = (Button) findViewById(R.id.btnInsertSQLite);
        btnInsertSQLite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertProduto();
            }
        });

        edtMenuNome = (EditText) findViewById(R.id.edtMenuTesteNome);
        edtMenuNome.setText("MenuDoDia");
    }

    private void insertProduto() {
        db_h.insertProduto("Banana","Banana","Banana","1","Banana",true);
    }

    private void getPreços() {
        int total = 0;

        Log.d(TAG, "Start printing getPreços\n");

        String menu = edtMenuNome.getText().toString();

        String selectQuery = "SELECT m.Nome, mp.PRODUTO_Id, p.Preço "
                + "FROM PRODUTO p, MENU m, MENU_PRODUTO mp "
                + "WHERE m.Nome = mp.MENU_Id "
                + "AND p.Nome = mp.PRODUTO_Id "
                + "AND m.Nome = '"+ menu + "'";

        SQLiteDatabase db = db_h.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String MENU_Id = cursor.getString(cursor.getColumnIndex("Nome"));
                String PRODUTO_Id = cursor.getString(cursor.getColumnIndex("PRODUTO_Id"));
                String Preço = cursor.getString(cursor.getColumnIndex("Preço"));
                total += Integer.parseInt(Preço);

                Log.d(TAG, "MENU_Id: " + MENU_Id + ", PRODUTO_Id: " + PRODUTO_Id + " => Preço: " + Preço + "\n");

            } while (cursor.moveToNext());
            Log.d(TAG, "TOTAL: " + total + "\n");

        }

        // close db connection
        db.close();
        Log.d(TAG, "Finished loading getPreços\n");
    }

    private void printTableMenuProduto() {
        Log.d(TAG, "Start printing printTableMenuProduto\n");

        String selectQuery = "SELECT * FROM MENU_PRODUTO";

        SQLiteDatabase db = db_h.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String MENU_Id = cursor.getString(cursor.getColumnIndex("MENU_Id"));
                String PRODUTO_Id = cursor.getString(cursor.getColumnIndex("PRODUTO_Id"));

                Log.d(TAG, "MENU_Id: " + MENU_Id + " => PRODUTO_Id: " + PRODUTO_Id + "\n");

            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();
        Log.d(TAG, "Finished loading printTableMenuProduto\n");
    }

    private void printTableMenu() {
        Log.d(TAG, "Start printing printTableMenu\n");

        String selectQuery = "SELECT * FROM MENU";

        SQLiteDatabase db = db_h.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String nome = cursor.getString(cursor.getColumnIndex("Nome"));
                Log.d(TAG, "Name: " + nome + "\n");

            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();
        Log.d(TAG, "Finished loading printTableMenu\n");
    }

    private void printTableProduto() {
        Log.d(TAG, "Start printing printTableProduto\n");

        //String selectQuery = "SELECT * FROM PRODUTO WHERE TIPO = 'Bebida'";
        String selectQuery = "SELECT * FROM PRODUTO";

        SQLiteDatabase db = db_h.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String nome = cursor.getString(cursor.getColumnIndex("Nome"));
                String preço = cursor.getString(cursor.getColumnIndex("Preço"));

                Log.d(TAG, "Name: " + nome + " => Preço: " + preço + "\n");

            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();
        Log.d(TAG, "Finished loading printTableProduto\n");
    }

    private void loadProdutos() {
        Log.d(TAG, "Start loading loadProdutos\n");

        //db_h.resetTable("PRODUTO");

        CollectionReference produtos = db.collection(COLLECTION_PRODUTOS);

        //Query query = produtos.whereEqualTo("Tipo", tipo);
        Query query = produtos.orderBy("Nome");

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        db_h.insertProduto(document.getId(), document.get("Desc").toString(), document.get("Tipo").toString(),
                                document.get("Preço").toString(),document.get("Imagem").toString(), false);
                    }

                    Log.d(TAG, "Finished loading loadProdutos\n");

                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
            }
        });
    }

    private void loadMenus() {
        Log.d(TAG, "Start loading loadMenus\n");
//        db_h.resetTable("MENU");
//        db_h.resetTable("MENU_PRODUTO");

        CollectionReference menusCR = db.collection(COLLECTION_MENUS);

        final Query query = menusCR.orderBy("Nome");

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        // Produtos is a array of Strings
                        ArrayList<String> produtos = (ArrayList<String>)document.get("Produtos");
                        db_h.insertMenuProduto(document.getId(), produtos);
                        //menus.add(new Menu(document.getId(), produtos, ""));
                    }
                    Log.d(TAG, "Finished loading loadMenus\n");

                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
            }
        });
    }

    private void ShowToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
