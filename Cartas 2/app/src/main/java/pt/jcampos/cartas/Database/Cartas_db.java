package pt.jcampos.cartas.Database;

public class Cartas_db {
    public static final String TABLE_PRODUTO = "PRODUTO";
    public static final String TABLE_MENU = "MENU";
    public static final String TABLE_RESERVA = "RESERVA";
    public static final String TABLE_REST = "REST";

    public static final String TABLE_MENU_PRODUTO = "MENU_PRODUTO";

    // Create table SQL query
    public static final String CREATE_TABLE_PRODUTO =
            "CREATE TABLE PRODUTO("
                    + " Id TEXT,"
                    + " Nome TEXT,"
                    + " Desc TEXT,"
                    + " Preço TEXT,"
                    + " Tipo TEXT,"
                    + " Imagem TEXT,"
                    + " Local  BOOLEAN DEFAULT (0) "
                    + ")";

    // Create table SQL query
    public static final String CREATE_TABLE_MENU =
            "CREATE TABLE MENU("
                    + " Id TEXT,"
                    + " Nome TEXT, "
                    + " Local  BOOLEAN DEFAULT (0) "
                    + ")";

    // Create table SQL query
    // I included collum ID for ordering porpusals only
    public static final String CREATE_TABLE_MENU_PRODUTO =
            "CREATE TABLE MENU_PRODUTO("
                    + " ID INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + " MENU_Id TEXT,"
                    + " PRODUTO_Id TEXT"
                    + ")";

    // Create table SQL query
    // I included collum ID for ordering porpusals only
    public static final String CREATE_TABLE_RESERVA =
            "CREATE TABLE RESERVA("
                    + " Id       INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + " Nome     STRING,"
                    + " Email    STRING,"
                    + " Menu     STRING,"
                    + " Pessoas  STRING,"
                    + " DataHora STRING,"
                    + " Estado   STRING,"
                    + " Local  BOOLEAN DEFAULT (0)"
                    + " )";

    // Create table SQL query
    public static final String CREATE_TABLE_REST =
            "CREATE TABLE REST("
                    + " Nome        STRING,"
                    + " Lotação     STRING,"
                    + " Coordenadas STRING"
                    + " )";

    public Cartas_db(){

    }
}
