package pt.jcampos.cartas.Produtos;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

import pt.jcampos.cartas.Database.DatabaseHelper;
import pt.jcampos.cartas.R;

public class ProductList extends Activity {
    private static final String TAG = "PRODUTCT_LIST::::...";

    private static final int PRODUCT_ADD_REQUEST_CODE = 0;
    private static final int PRODUCT_UPDATE_REQUEST_CODE = 0;

    private Button btnAdd;
//    private FloatingActionButton fabAdd;
    private RadioGroup rdgTipo;

    private FirebaseFirestore dbf;
    private DatabaseHelper dbh;

    private ListView lvProducts;
    private ProductViewAdapter productViewAdapter;
    ArrayList<Product> products;

    // TODO: 14/12/2018
    // Add a Floating Action Button for creating new product action

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO: 13/12/2018
        // pesquisar sobre melhor forma de aliviar o prcessamento neste metodo
        // talvez desviar para outro as instruções mais demoradas

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        // TODO: 13/12/2018
        // Seria muito proveitoso usar a referência ao base de forma global
        dbf = FirebaseFirestore.getInstance();
        dbh = new DatabaseHelper(this);

        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(ProductList.this, ProductAdd.class);
                newIntent.putExtra("ProductID", "");

                startActivityForResult(newIntent, PRODUCT_ADD_REQUEST_CODE);
            }
        });

//        fabAdd = (FloatingActionButton) findViewById(R.id.fabAddProduct);
//        fabAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent newIntent = new Intent(ProductList.this, ProductAdd.class);
//                startActivityForResult(newIntent, PRODUCT_ADD_REQUEST_CODE);
//            }
//        });

        rdgTipo = (RadioGroup) findViewById(R.id.rgTipo);
        rdgTipo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);

                loadProdutos(rb.getText().toString());
            }
        });

        lvProducts = (ListView) findViewById(R.id.lvProducts);
        lvProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent newIntent = new Intent(ProductList.this, ProductAdd.class);
                Product product = (Product) productViewAdapter.getItem(position);
                newIntent.putExtra("ProductID", product.getId());
                startActivityForResult(newIntent, PRODUCT_UPDATE_REQUEST_CODE);
            }
        });
        loadProdutos("Prato");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) { // by definition Activity.RESULT_CANCELED = -1
//            return;
        }
        if (resultCode == RESULT_OK) { // by definition Activity.RESULT_OK = 0
            // get String data from Intent
            String returnString = data.getStringExtra("NewProduct");
            // update list info
            int selectedId = rdgTipo.getCheckedRadioButtonId();
            // find the radiobutton by returned id
            RadioButton rb = (RadioButton) findViewById(selectedId);
            loadProdutos(rb.getText().toString());
        }
    }

    private void loadProdutos(String tipo) {
        Log.d(TAG, "Start loadMenus\n");
        products = new ArrayList<Product>();

        String query = "SELECT p.* "
                + "FROM PRODUTO p "
                + "WHERE p.Tipo = '"+ tipo +"' "
                + "ORDER By p.Nome ";

        SQLiteDatabase db = dbh.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String produtoId = cursor.getString(cursor.getColumnIndex("Nome"));
                String preço = cursor.getString(cursor.getColumnIndex("Preço"));

                products.add(new Product(produtoId, preço, tipo));

            } while (cursor.moveToNext());
            Log.d(TAG, "finishing loadMenus\n");
        }

        productViewAdapter = new ProductViewAdapter(products);
        lvProducts.setAdapter(productViewAdapter);
        // close db connection
        db.close();
        Log.d(TAG, "Finished loadMenus\n");
    }

}

