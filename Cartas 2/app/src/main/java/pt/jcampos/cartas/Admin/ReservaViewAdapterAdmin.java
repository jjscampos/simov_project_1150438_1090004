package pt.jcampos.cartas.Admin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import pt.jcampos.cartas.Client.Reserva;
import pt.jcampos.cartas.R;

public class ReservaViewAdapterAdmin extends BaseAdapter {
    private static final String TAG = "ReservaViewAdapterAdmin:::... ";

    private final List<Reserva> items;

    public ReservaViewAdapterAdmin(final List<Reserva> items) {
        this.items = items;
    }

    public int getCount() {
        return this.items.size();
    }

    public Object getItem(int arg0) {
        return this.items.get(arg0);
    }

    public long getItemId(int arg0) {
        return arg0;
    }

    public View getView(int arg0, View arg1, ViewGroup arg2) {
        // TODO Auto-generated method stub
        final Reserva row = this.items.get(arg0);
        View itemView = null;

        if (arg1 == null) {
            LayoutInflater inflater = (LayoutInflater) arg2.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.reserva_admin_item, null);
        } else {
            itemView = arg1;
        }

        // Set the text of the row
        TextView tvDataHora = (TextView) itemView.findViewById(R.id.tvReservaAdminItemDataHora);
        tvDataHora.setText(row.getDataHora());

        TextView tvMenu = (TextView) itemView.findViewById(R.id.tvReservaAdminItemMenu);
        tvMenu.setText(row.getMenuId());

        CheckBox cbEstado = itemView.findViewById(R.id.cbReservaAdminItemEstado);
//        cbEstado.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                Log.d(TAG, "setOnCheckedChangeListener: "+ isChecked +"\n");
//
//                if (isChecked) row.setEstado("OK");
//                else row.setEstado("NOK");
//            }
//        });

        if (row.getEstado().equals("OK")) cbEstado.setChecked(true);

        TextView tvEMail = (TextView) itemView.findViewById(R.id.tvReservaAdminItemEmail);
        tvEMail.setText(row.getEmail());

        return itemView;
    }
}
