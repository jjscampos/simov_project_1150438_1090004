package pt.jcampos.cartas.Utils;

public class Global {
    //public static final String APP_DIR = Base.getContext().getApplicationInfo().dataDir;
    public static final String TEMP_IMAGE_DIR = "/Imagens/";
    public static final int EXPORT_IMAGE_QUALITY = 75;

    // FirebaseFiresotre Collections
    public static final String COLLECTION_ADMIN = "ADMINS";
    public static final String COLLECTION_PRODUTOS = "PRODUTOS";
    public static final String COLLECTION_MENUS = "MENUS";
    public static final String COLLECTION_RESERVAS = "RESERVAS";
    public static final String COLLECTION_REST = "REST";

    //
    public static final String NO_RAW_QUERY_RECORS = "NÃO EXISTEM REGISTOS NOVOS";
    public static final String NO_CONNECTIVITY = "NÃO TEMOS CONNECTICIDADE! TENTE MAIS TARDE!";
    public static final String VERIFIQUE_DADOS_RESERVA = "Verifque os dados do pedido reserva";

    // predefined user types
    public static final String USER_ADMIN = "admin";
    public static final String USER_CLIENT = "client";

    public static final String RESERVA_ESTADO_ESPERA = "WAIT";
    public static final String MSG_PEDIDOS_RESERVA_ATUALIZADOS = "Pedidos de Reserva atualizados";
    public static final String MSG_TERMINOU_SESSAO_GOOGLE = "Terminou sessão no GOOGLE tenta novamente!: ";
    public static final String MSG_GMAPS_INDISPONIVEL = "Não foi possivel iniciar o GMAPS!";
}
