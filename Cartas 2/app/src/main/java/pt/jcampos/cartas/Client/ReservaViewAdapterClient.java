package pt.jcampos.cartas.Client;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import pt.jcampos.cartas.R;

public class ReservaViewAdapterClient extends BaseAdapter {
    private final List<Reserva> items;

    public ReservaViewAdapterClient(final List<Reserva> items) {
        this.items = items;
    }

    public int getCount() {
        return this.items.size();
    }

    public Object getItem(int arg0) {
        return this.items.get(arg0);
    }

    public long getItemId(int arg0) {
        return arg0;
    }

    public View getView(int arg0, View arg1, ViewGroup arg2) {
        // TODO Auto-generated method stub
        final Reserva row = this.items.get(arg0);
        View itemView = null;

        if (arg1 == null) {
            LayoutInflater inflater = (LayoutInflater) arg2.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.reserva_item, null);
        } else {
            itemView = arg1;
        }

        // Set the text of the row
        TextView tvDataHora = (TextView) itemView.findViewById(R.id.tvReservaItemDataHora);
        tvDataHora.setText(row.getDataHora());

        TextView tvMenu = (TextView) itemView.findViewById(R.id.tvReservaItemMenu);
        tvMenu.setText(row.getMenuId());

        TextView tvEstado = (TextView) itemView.findViewById(R.id.tvReservaItemEstado);
        tvEstado.setText(row.getEstado());

        return itemView;
    }
}
