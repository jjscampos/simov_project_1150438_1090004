package pt.jcampos.cartas.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import pt.jcampos.cartas.Utils.Global;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "DatabaseHelper:::... ";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "cartas_db.sqlite";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // create tables
        Log.d(TAG, "onCreate\n");

//        db.execSQL("DROP TABLE IF EXISTS " + Cartas_db.TABLE_PRODUTO);
//        db.execSQL("DROP TABLE IF EXISTS " + Cartas_db.TABLE_MENU);
//        db.execSQL("DROP TABLE IF EXISTS " + Cartas_db.TABLE_MENU_PRODUTO);
//        db.execSQL("DROP TABLE IF EXISTS " + Cartas_db.TABLE_RESERVA);
//        db.execSQL("DROP TABLE IF EXISTS " + Cartas_db.TABLE_REST);

        db.execSQL(Cartas_db.CREATE_TABLE_PRODUTO);
        db.execSQL(Cartas_db.CREATE_TABLE_MENU);
        db.execSQL(Cartas_db.CREATE_TABLE_MENU_PRODUTO);
        db.execSQL(Cartas_db.CREATE_TABLE_RESERVA);
        db.execSQL(Cartas_db.CREATE_TABLE_REST);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
//        db.execSQL("DROP TABLE IF EXISTS " + Cartas_db.TABLE_PRODUTO);
//        db.execSQL("DROP TABLE IF EXISTS " + Cartas_db.TABLE_MENU);
//        db.execSQL("DROP TABLE IF EXISTS " + Cartas_db.TABLE_MENU_PRODUTO);
//        db.execSQL("DROP TABLE IF EXISTS " + Cartas_db.TABLE_RESERVA);
//        db.execSQL("DROP TABLE IF EXISTS " + Cartas_db.TABLE_REST);
        resetTables();
        // Create tables again
        //onCreate(db);
    }

    public void resetTables() {
        SQLiteDatabase db = this.getWritableDatabase();

        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + Cartas_db.TABLE_PRODUTO);
        db.execSQL("DROP TABLE IF EXISTS " + Cartas_db.TABLE_MENU);
        db.execSQL("DROP TABLE IF EXISTS " + Cartas_db.TABLE_MENU_PRODUTO);
        db.execSQL("DROP TABLE IF EXISTS " + Cartas_db.TABLE_RESERVA);
        db.execSQL("DROP TABLE IF EXISTS " + Cartas_db.TABLE_REST);

        // Create tables again
        onCreate(db);
    }

    public long insertProduto(String Nome, String Desc, String Tipo, String Preço, String Imagem, boolean Local) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("Id", Nome);
        values.put("Nome", Nome);
        values.put("Desc", Desc);
        values.put("Tipo", Tipo);
        values.put("Preço", Preço);
        values.put("Imagem", Imagem);
        values.put("Local", Local ? 1:0); // Local must be a number: 0 if false or 1 if true

        // insert row
        long id = db.insert(Cartas_db.TABLE_PRODUTO, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public long insertMenuProduto(String Nome, ArrayList<String> produtos) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        // no need to add them
        values.put("Id", Nome);
        values.put("Nome", Nome);

        // insert row
        long id = db.insert(Cartas_db.TABLE_MENU, null, values);

        values.clear();
        for (String prd : produtos) {
            values.put("MENU_Id", Nome);
            values.put("PRODUTO_Id", prd);
            long ids = db.insert(Cartas_db.TABLE_MENU_PRODUTO, null, values);
        }

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public long insertReserva(String Nome, String Email, String menuID, String dataHora, String numPessoas, String estado) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        // no need to add them
        values.put("Nome", Nome);
        values.put("Email", Email);
        values.put("Menu", menuID);
        values.put("Pessoas", numPessoas);
        values.put("DataHora", dataHora);
        values.put("Estado", estado);

        // insert row
        long id = db.insert(Cartas_db.TABLE_RESERVA, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public long insertPedidoReserva(String Nome, String Email, String menuID, String dataHora, String numPessoas) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        // no need to add them
        values.put("Nome", Nome);
        values.put("Email", Email);
        values.put("Menu", menuID);
        values.put("Pessoas", numPessoas);
        values.put("DataHora", dataHora);
        values.put("Estado", Global.RESERVA_ESTADO_ESPERA);
        values.put("Local", 1);

        // insert row
        long id = db.insert(Cartas_db.TABLE_RESERVA, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public void updateProduto(String Nome, String Desc, String Tipo, String Preço, String Imagem, boolean Local) {
        // get writable database as we want to write data
        String query = String.format("UPDATE PRODUTO SET "
                + " Nome = '%s',"
                + " Desc = '%s',"
                + " Tipo = '%s',"
                + " Preço = '%s',"
                + " Imagem = '%s',"
                + " Local = 1"
                + " WHERE id = '%s'", Nome, Desc, Tipo, Preço, Imagem, Nome);

        Log.d(TAG, "updateProduto query: "+ query +"\n");

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
        db.close();
    }

    public void setLocalProductUpdated(String produtoId, String Imagem) {
        String query = String.format("UPDATE PRODUTO SET "
                    + " Local = 0,"
                    + " Imagem = '%s'"
                    + " WHERE id = '%s'", Imagem, produtoId);

        Log.d(TAG, "setProductUpdated query: "+ query +"\n");

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
        db.close();
    }

    public void setLocalReservasUpdated(String id) {
        String query = String.format("UPDATE RESERVA SET "
                + " Local = 0,"
                + " Estado = 'Wait'"
                + " WHERE id = '%s'", id);

        Log.d(TAG, "setLocalReservasUpdated query: "+ query +"\n");

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
        db.close();
    }

    public void setLocalReservaEstadoUpdate(String id, String estado) {
        String query = String.format("UPDATE RESERVA SET "
                + " Local = 1,"
                + " Estado = '%s'"
                + " WHERE id = '%s'", estado ,id);

        Log.d(TAG, "setLocalReservaEstadoUpdate query: "+ query +"\n");

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
        db.close();
    }
}
