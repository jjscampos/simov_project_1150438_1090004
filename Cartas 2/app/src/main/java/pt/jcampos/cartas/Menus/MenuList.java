package pt.jcampos.cartas.Menus;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import pt.jcampos.cartas.Client.ReservaPedir;
import pt.jcampos.cartas.Database.DatabaseHelper;
import pt.jcampos.cartas.R;
import pt.jcampos.cartas.Utils.Global;

public class MenuList extends Activity {
    private static final String TAG = "MENU_LIST::::...";
    private static final int MENU_ADD_REQUEST_CODE = 0;
    private static final int MENU_UPDATE_REQUEST_CODE = 0;

    private Button btnAdd;

    //private FirebaseFirestore f_db;
    private DatabaseHelper dbh;

    private ListView lvMenus;

    private MenuViewAdapter menuViewAdapter;
    private ArrayList<Menu> menus;
    private String finalPrice;

    private boolean userAdmin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_list);

        // TODO: 13/12/2018
        // Seria muito proveitoso usar a referência ao base de forma global
        //f_db = FirebaseFirestore.getInstance();
        dbh = new DatabaseHelper(this);

        btnAdd = (Button) findViewById(R.id.btnAddMenu);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(MenuList.this, MenuAdd.class);
                newIntent.putExtra("MenuID", "NewMenu");

                startActivityForResult(newIntent, MENU_ADD_REQUEST_CODE);
            }
        });

        lvMenus = (ListView) findViewById(R.id.lvMenus);
        lvMenus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Menu menu = (Menu) menuViewAdapter.getItem(position);
                if (userAdmin) {
                    Intent newIntent = new Intent(MenuList.this, MenuAdd.class);
                    newIntent.putExtra("MenuID", menu.getId());
                    startActivityForResult(newIntent, MENU_UPDATE_REQUEST_CODE);
                }else{
                    Intent newIntent = new Intent(MenuList.this, ReservaPedir.class);
                    newIntent.putExtra("MenuID", menu.getId());
                    startActivity(newIntent);
                }
            }
        });

        userAdmin = getIntent().getStringExtra("UserType").equals(Global.USER_ADMIN);
        if (!userAdmin) btnAdd.setVisibility(View.INVISIBLE);
        btnAdd.bringToFront();

        Log.d(TAG, "ADMIN?" + userAdmin);

        loadMenus();

        menuViewAdapter = new MenuViewAdapter(menus);
        lvMenus.setAdapter(menuViewAdapter);
    }

    private void loadMenus() {
        Log.d(TAG, "Start loadMenus\n");
        menus = new ArrayList<Menu>();

        String query = "SELECT m.Nome "
                    + "FROM MENU m "
                    + "ORDER By m.Nome ";

        SQLiteDatabase db = dbh.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                String MenuId = cursor.getString(cursor.getColumnIndex("Nome"));

                loadProdutos(MenuId);

            } while (cursor.moveToNext());
            Log.d(TAG, "finishing loadMenus\n");
        }
        // close db connection
        db.close();
        Log.d(TAG, "Finished loadMenus\n");
    }

    private void loadProdutos(String menuId) {
        int total = 0;
        ArrayList<String> produtos = new ArrayList<>();
        Log.d(TAG, "Start printing getPreços\n");

        String selectQuery = "SELECT m.Nome, mp.PRODUTO_Id, p.Preço "
                + "FROM PRODUTO p, MENU m, MENU_PRODUTO mp "
                + "WHERE m.Nome = mp.MENU_Id "
                + "AND p.Nome = mp.PRODUTO_Id "
                + "AND m.Nome = '" + menuId + "' "
                + "ORDER BY mp.ID";

        SQLiteDatabase db = dbh.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String ProdutoID = cursor.getString(cursor.getColumnIndex("PRODUTO_Id"));
                String Preço = cursor.getString(cursor.getColumnIndex("Preço"));
                total += Integer.parseInt(Preço);

                Log.d(TAG, "MENU_Id: " + menuId + ", PRODUTO_Id: " + ProdutoID + " => Preço: " + Preço + "\n");
                produtos.add(ProdutoID);
            } while (cursor.moveToNext());
            menus.add(new Menu(menuId, produtos, Integer.toString(total)));

            Log.d(TAG, "TOTAL do menu => " + total + "\n");

        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) { // by definition Activity.RESULT_CANCELED = -1
//            return;
        }
        if (resultCode == RESULT_OK) { // by definition Activity.RESULT_OK = 0
//            // get String data from Intent
//            // find the radiobutton by returned id
//            RadioButton rb = (RadioButton) findViewById(selectedId);
//            listFromQuery(rb.getText().toString());
//        }
        }
    }

    private void ShowToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
