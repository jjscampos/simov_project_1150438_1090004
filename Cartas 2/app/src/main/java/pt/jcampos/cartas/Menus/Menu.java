package pt.jcampos.cartas.Menus;

import java.util.ArrayList;

public class Menu {
    private String id, preço;
    private ArrayList<String> produtos;

    public String getId() { return id; }

    public String getPreço() { return preço; }

    public  ArrayList<String> getProdutos() {
        return produtos;
    }

    public Menu (String id, ArrayList<String> produtos, String preço)
    {
        this.id = id;
        this.produtos = produtos;
        this.preço = preço;
    }
}
