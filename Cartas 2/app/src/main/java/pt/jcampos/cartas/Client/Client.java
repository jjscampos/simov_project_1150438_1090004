package pt.jcampos.cartas.Client;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pt.jcampos.cartas.Auth;
import pt.jcampos.cartas.Database.DatabaseHelper;
import pt.jcampos.cartas.Menus.MenuList;
import pt.jcampos.cartas.R;
import pt.jcampos.cartas.Utils.Global;

public class Client extends Activity {
    private static final String TAG = "CLIENT::::... ";

    private FirebaseFirestore ff_db;

    private DatabaseHelper dbh;

    private Button btnClientManus;

    private ArrayList<Reserva> reservas;

    private ListView lvReservas;
    private ReservaViewAdapterClient reservaViewAdapterClient;

    private String Nome, Email, restLatitude, restLongitude, restNome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);

        whoAmI();

        ff_db = FirebaseFirestore.getInstance();

        dbh = new DatabaseHelper(this);

        btnClientManus = (Button) findViewById(R.id.btnClientMenus);
        btnClientManus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(Client.this, MenuList.class);
                newIntent.putExtra("UserType", Global.USER_CLIENT);
                startActivity(newIntent);
            }
        });

        Button btnClientMap = (Button) findViewById(R.id.btnClientMap);
        btnClientMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDriving();
            }
        });

        Button btnClientSubmeterReservas = findViewById(R.id.btnClientSubmeterReservas);
        btnClientSubmeterReservas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateReservas();
            }
        });

        lvReservas = findViewById(R.id.lvReservasClient);

        dbh.resetTables();
        loadProdutos();
        loadMenus();
        loadReservas();
        loadRestInfo();
    }

    private void startDriving() {
        //testing propusals: 41.533838, -8.790278
        String uri = "http://maps.google.com/maps?daddr=" + restLatitude + "," + restLongitude + " (" + restNome + ")";

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setPackage("com.google.android.apps.maps");
        try
        {
            startActivity(intent);
        }
        catch(ActivityNotFoundException ex)
        {
            try
            {
                Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(unrestrictedIntent);
            }
            catch(ActivityNotFoundException innerEx)
            {
                ShowToast(Global.MSG_GMAPS_INDISPONIVEL);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadReservasIntoListView();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        switch(action & MotionEvent.ACTION_MASK)
        {
            case MotionEvent.ACTION_POINTER_DOWN:
                int count = event.getPointerCount();
                if (count>2) { // Number of 'fingers' in this time, multitouch touch down
                    Auth.doGoogleSignOut(this);
                    ShowToast(Global.MSG_TERMINOU_SESSAO_GOOGLE);
                    finish();
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    private void updateReservas() {
        Log.d(TAG, "Start updateReservas\n");

        String query = "SELECT r.* "
                + " FROM RESERVA r"
                + " WHERE r.Local = 1"
                + " AND r.Email = '"+ Email + "'"
                + " ORDER By r.DataHora";

        SQLiteDatabase db = dbh.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount()==0)
            ShowToast(Global.NO_RAW_QUERY_RECORS);

        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getString(cursor.getColumnIndex("Id"));
                String nome = cursor.getString(cursor.getColumnIndex("Nome"));
                String menu = cursor.getString(cursor.getColumnIndex("Menu"));
                String pessoas = cursor.getString(cursor.getColumnIndex("Pessoas"));
                String dataHora = cursor.getString(cursor.getColumnIndex("DataHora"));
                String estado = cursor.getString(cursor.getColumnIndex("Estado"));

                updateOnline(id, nome, menu, pessoas, dataHora, estado);

            } while (cursor.moveToNext());
            Log.d(TAG, "finishing updateReservas\n");
        }
    }

    private void updateOnline(final String id, String nome, String menu, String pessoas, final String dataHora, String estado) {
        Log.d(TAG, "Start updateOnline\n");

        Map<String, Object> newProduct = new HashMap<>();
        newProduct.put("Nome", nome);
        newProduct.put("Email", Email);
        newProduct.put("Menu", menu);
        newProduct.put("Pessoas", pessoas);
        newProduct.put("DataHora", dataHora);
        newProduct.put("Estado", estado);

        DocumentReference docRef = ff_db.collection(Global.COLLECTION_RESERVAS).document();
        docRef.set(newProduct).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dbh.setLocalReservasUpdated(id);
                ShowToast(Global.MSG_PEDIDOS_RESERVA_ATUALIZADOS);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                ShowToast("Erro ao atualizar: " + e.toString());
            }
        });

        Log.d(TAG, "finishing updateOnline\n");
        // EventListener for record events
    }

    private void loadRestInfo() {
        Log.d(TAG, "Start loading loadRestInfo\n");

        CollectionReference produtos = ff_db.collection(Global.COLLECTION_REST);

        Query query = produtos.orderBy("Nome");

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        restNome = document.get("Nome").toString();
                        String coordenadas = document.get("Coordenadas").toString();
                        String[] coord = coordenadas.split(",");
                        restLatitude = coord[0];
                        restLongitude = coord[1];
                    }
                    Log.d(TAG, "Finished loading loadRestInfo\n");

                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
            }
        });
    }

    private void loadProdutos() {
        Log.d(TAG, "Start loading loadProdutos\n");

        CollectionReference produtos = ff_db.collection(Global.COLLECTION_PRODUTOS);

        Query query = produtos.orderBy("Nome");

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        dbh.insertProduto(document.getId(), document.get("Desc").toString(), document.get("Tipo").toString(),
                                document.get("Preço").toString(),document.get("Imagem").toString(), false);
                    }
                    Log.d(TAG, "Finished loading loadProdutos\n");

                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
            }
        });
    }

    private void loadMenus() {
        Log.d(TAG, "Start loading loadMenus\n");

        CollectionReference menusCR = ff_db.collection(Global.COLLECTION_MENUS);

        final Query query = menusCR.orderBy("Nome");

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        // Produtos is a array of Strings
                        ArrayList<String> produtos = (ArrayList<String>)document.get("Produtos");
                        dbh.insertMenuProduto(document.getId(), produtos);
                    }
                    btnClientManus.setEnabled(true);
                    Log.d(TAG, "Finished loading loadMenus\n");

                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
            }
        });
    }

    private void loadReservas() {
        Log.d(TAG, "Start loading loadReservas\n");

        CollectionReference menusCR = ff_db.collection(Global.COLLECTION_RESERVAS);

        //final Query query = menusCR.orderBy("Nome");
        final Query query = menusCR.whereEqualTo("Email", Email);

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {

                        dbh.insertReserva(document.get("Nome").toString(),document.get("Email").toString(),
                                document.get("Menu").toString(),document.get("DataHora").toString(),document.get("Pessoas").toString(),
                                document.get("Estado").toString());
                    }
                    loadReservasIntoListView();
                    Log.d(TAG, "Finished loading loadReservas\n");

                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
            }
        });
    }

    private void loadReservasIntoListView() {
        Log.d(TAG, "Start loadReservas\n");
        reservas = new ArrayList<Reserva>();

        String query = "SELECT r.* "
                + "FROM RESERVA r "
                + "ORDER By r.DataHora ";

        SQLiteDatabase db = dbh.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String dataHora = cursor.getString(cursor.getColumnIndex("DataHora"));
                String menuId = cursor.getString(cursor.getColumnIndex("Menu"));
                String estado = cursor.getString(cursor.getColumnIndex("Estado"));

                reservas.add(new Reserva(dataHora, estado, "", "", menuId, ""));
                Log.d(TAG, "Add reservas: " + dataHora + ", " + menuId + ", " + estado + "\n");

            } while (cursor.moveToNext());
            Log.d(TAG, "finishing Add reservas" + reservas.size() + "\n");
        }

        reservaViewAdapterClient = new ReservaViewAdapterClient(reservas);
        lvReservas.setAdapter(reservaViewAdapterClient);
        // close db connection
        db.close();
        Log.d(TAG, "Finished loadReservas\n");
    }

    private void whoAmI() {
        Log.d(TAG,"whoAmI:");
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        Email = account.getEmail();
        Nome = account.getDisplayName();
    }

    private void ShowToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
