package pt.jcampos.cartas.Menus;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import pt.jcampos.cartas.Database.DatabaseHelper;
import pt.jcampos.cartas.R;

public class ListViewFragment extends Fragment {
    private static final String TAG = "ListViewFragment::::...";

//    private FirebaseFirestore db;
    private DatabaseHelper dbh;

    private Context context;

    public ListViewAdapter adapter;
    private ArrayList<String> arrayList;
    private String tipo;
    private String selectedItem = "";

    public ListViewFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_view_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        dbh = new DatabaseHelper(getContext());

        //listFromQuery(view);

        loadProdutos(view);
    }

    private void loadProdutos(View view) {
        Log.d(TAG, "Start loadProdutos\n");

        ListView listView = (ListView) view.findViewById(R.id.list_view);
        arrayList = new ArrayList<>();

        String query = "SELECT p.* "
                + "FROM PRODUTO p "
                + "WHERE p.Tipo = '" + tipo + "' "
                + "ORDER By p.Nome ";
        Log.d(TAG, "query: "+ query +"\n");

        SQLiteDatabase db = dbh.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String produtoId = cursor.getString(cursor.getColumnIndex("Nome"));
                String preço = cursor.getString(cursor.getColumnIndex("Preço"));
                arrayList.add(produtoId);

            } while (cursor.moveToNext());

            adapter = new ListViewAdapter(context, arrayList, selectedItem);
            adapter.setItemChecked(new ListViewAdapter.ItemChecked() {
                @Override
                public void onItemChecked(String name) {
                    selectedItem = name;
                    Log.d(TAG, "FireListener:onItemChecked for "+ tipo +" With selectedItem = " + selectedItem);
                }
            });
            listView.setAdapter(adapter);
        }

        // close db connection
        db.close();
        Log.d(TAG, "Finished loadProdutos\n");
    }

    /*
     *
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /*
    *
    */
    public String getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(String produto) {
        selectedItem = produto;
        //adapter.setSelectedItem(produto);
    }
}
