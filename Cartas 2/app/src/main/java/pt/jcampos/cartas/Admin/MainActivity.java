package pt.jcampos.cartas.Admin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pt.jcampos.cartas.Auth;
import pt.jcampos.cartas.Client.Reserva;
import pt.jcampos.cartas.Database.DatabaseHelper;
import pt.jcampos.cartas.Menus.MenuList;
import pt.jcampos.cartas.Produtos.ProductList;
import pt.jcampos.cartas.R;
import pt.jcampos.cartas.Utils.Global;

public class MainActivity extends Activity {
    private static final String TAG = "MAIN_ACTIVITY:::... ";

    // Add a different request code for every activity you are starting from here
    private static final int REQUEST_CODE_PRODUCT_ADD = 1;
    private static final int REQUEST_CODE_MENU_ADD = 2;
    private static final int REQUEST_CODE_CARTA_ADD = 3;

    private Button btnProducts, btnMenus;

    private FirebaseFirestore ff_db;
    private FirebaseStorage storage;
    private StorageReference storageReference;

    private DatabaseHelper dbh;

    private String imageURI = "";

    private ArrayList<Reserva> reservas;
    private ReservaViewAdapterAdmin reservaViewAdapterAdmin;
    private ListView lvReservasAdmin;


    /*
    TODO: 12/12/2018
    adicionar restantes metodos override onStar, onStop, onResmo.
    Estudar o impacto e vantagens na sua utilização! :-)
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ff_db = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        dbh = new DatabaseHelper(this);

        btnProducts = (Button) findViewById(R.id.btnProducts);
        btnProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                TODO: 12/12/2018
                Verificar o requestcode em função de atuar no resumo de existencias neste layout
                */
                Intent newIntent = new Intent(MainActivity.this, ProductList.class);
                startActivityForResult(newIntent, REQUEST_CODE_PRODUCT_ADD);
            }
        });

        btnMenus = (Button) findViewById(R.id.btnMenus);
        btnMenus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(MainActivity.this, MenuList.class);
                newIntent.putExtra("UserType", Global.USER_ADMIN);
                startActivityForResult(newIntent, REQUEST_CODE_MENU_ADD);
            }
        });

        lvReservasAdmin = (ListView) findViewById(R.id.lvReservasAdmin);

        lvReservasAdmin.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemClick(AdapterView<?>\n");

                Reserva reserva = (Reserva) reservaViewAdapterAdmin.getItem(position);
                updateReservaState(reserva);
            }
        });

        Button btnUpdateFirestore = (Button) findViewById(R.id.btnUpdateFirestore);
        btnUpdateFirestore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProdutos();
            }
        });
        btnUpdateFirestore.bringToFront();

        dbh.resetTables();
        loadProdutos();
        loadMenus();

        loadReservas();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_CANCELED) { // by definition Activity.RESULT_CANCELED = -1
//            tvDebugInfo.append("onActivityResult: RESULT_CANCELED = " + RESULT_CANCELED);
//            return;
        }
        if (resultCode == RESULT_OK) { // by definition Activity.RESULT_OK = 0
//            String returnString = data.getStringExtra("NewProduct");
//            tvDebugInfo.append("onActivityResult: RESULT_OK =" + RESULT_OK);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        switch(action & MotionEvent.ACTION_MASK)
        {
            case MotionEvent.ACTION_POINTER_DOWN:
                int count = event.getPointerCount();
                if (count > 2) { // Number of 'fingers' in this time, multitouch touch down
                    Auth.doGoogleSignOut(this);
                    ShowToast(Global.MSG_TERMINOU_SESSAO_GOOGLE);
                    finish();
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    private void loadProdutos() {
        Log.d(TAG, "Start loading loadProdutos\n");

        CollectionReference produtos = ff_db.collection(Global.COLLECTION_PRODUTOS);

        Query query = produtos.orderBy("Nome");

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        dbh.insertProduto(document.getId(), document.get("Desc").toString(), document.get("Tipo").toString(),
                                document.get("Preço").toString(),document.get("Imagem").toString(), false);
                    }
                    btnProducts.setEnabled(true);
                    Log.d(TAG, "Finished loading loadProdutos\n");

                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
            }
        });
    }

    private void loadMenus() {
        Log.d(TAG, "Start loading loadMenus\n");

        CollectionReference menusCR = ff_db.collection(Global.COLLECTION_MENUS);

        final Query query = menusCR.orderBy("Nome");

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        // Produtos is a array of Strings
                        ArrayList<String> produtos = (ArrayList<String>)document.get("Produtos");
                        dbh.insertMenuProduto(document.getId(), produtos);
                    }
                    btnMenus.setEnabled(true);
                    Log.d(TAG, "Finished loading loadMenus\n");

                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
            }
        });
    }

    private void loadReservas() {
        Log.d(TAG, "Start loading loadReservas\n");

        CollectionReference menusCR = ff_db.collection(Global.COLLECTION_RESERVAS);

        final Query query = menusCR.orderBy("Email");
        //final Query query = menusCR.whereEqualTo("Email", Email);

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {

                        dbh.insertReserva(document.get("Nome").toString(),document.get("Email").toString(),
                                document.get("Menu").toString(),document.get("DataHora").toString(),document.get("Pessoas").toString(),
                                document.get("Estado").toString());
                    }
                    loadReservasIntoListView();
                    Log.d(TAG, "Finished loading loadReservas\n");

                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
            }
        });
    }

    private void loadReservasIntoListView() {
        Log.d(TAG, "Start loadReservas\n");
        reservas = new ArrayList<Reserva>();

        String query = "SELECT r.* "
                + "FROM RESERVA r "
                + "ORDER By r.DataHora ";

        SQLiteDatabase db = dbh.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getString(cursor.getColumnIndex("Id"));
                String dataHora = cursor.getString(cursor.getColumnIndex("DataHora"));
                String menuId = cursor.getString(cursor.getColumnIndex("Menu"));
                String estado = cursor.getString(cursor.getColumnIndex("Estado"));
                String email = cursor.getString(cursor.getColumnIndex("Email"));

                Reserva newReserva = new Reserva(dataHora, estado, email, "", menuId, "");
                newReserva.setId(id);
                reservas.add(newReserva);

                Log.d(TAG, "Add reservas: " + dataHora + ", " + menuId + ", " + estado + "\n");

            } while (cursor.moveToNext());
            Log.d(TAG, "finishing Add reservas" + reservas.size() + "\n");
        }

        reservaViewAdapterAdmin = new ReservaViewAdapterAdmin(reservas);
        lvReservasAdmin.setAdapter(reservaViewAdapterAdmin);

        // close db connection
        db.close();
        Log.d(TAG, "Finished loadReservas\n");
    }

    private void updateReservaState(Reserva reserva) {
        Log.d(TAG, "Start updateReservaState\n");

        if (!reserva.getEstado().equals("OK")) reserva.setEstado("OK");
        if (reserva.getEstado().equals("OK")) reserva.setEstado("NOK");

        dbh.setLocalReservaEstadoUpdate(reserva.getId(), reserva.getEstado());
        reservaViewAdapterAdmin.notifyDataSetChanged();

        //loadReservasIntoListView();
        Log.d(TAG, "Finished updateReservaState\n");
    }

    private void updateProdutos() {
        Log.d(TAG, "Start updateProdutos\n");

        String query = "SELECT p.* "
                + " FROM PRODUTO p"
                + " WHERE p.Local = 1"
                + " ORDER By p.Nome";

        SQLiteDatabase db = dbh.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount()==0)
            ShowToast(Global.NO_RAW_QUERY_RECORS);

        if (cursor.moveToFirst()) {
            do {
                String produtoId = cursor.getString(cursor.getColumnIndex("Nome"));
                String preço = cursor.getString(cursor.getColumnIndex("Preço"));
                String desc = cursor.getString(cursor.getColumnIndex("Desc"));
                String imagem = cursor.getString(cursor.getColumnIndex("Imagem"));
                // get imagem uri
                String tipo = cursor.getString(cursor.getColumnIndex("Tipo"));

                updateOnline(produtoId, desc, tipo, preço, imagem);

            } while (cursor.moveToNext());
            Log.d(TAG, "finishing updateProdutos\n");
        }
    }

    private void updateReservas() {
        Log.d(TAG, "Start updateReservas\n");
        // Não consegui fazer este a tempo:
        // Sorry
    }

    private void updateOnline(String produtoId, String desc, String tipo, String preço, String imagem) {
        Log.d(TAG, "Start updateOnline\n");

        //updateDB(produtoId, desc, tipo, preço, imagem);

        if (imagem.startsWith("https://firebasestorage.googleapis.com"))
            updateDB(produtoId, desc, tipo, preço, imagem);
        else
            uploadImage(produtoId, desc, tipo, preço, imagem);
        Log.d(TAG, "finishing updateOnline\n");
    }

    private void updateDB(final String produtoId, String desc, String tipo, String preço, final String imagem) {
        Log.d(TAG, "Start updateDB\n");

        Map<String, Object> newProduct = new HashMap<>();
        newProduct.put("Tipo", tipo);
        newProduct.put("Imagem", imagem);
        newProduct.put("Nome", produtoId);
        newProduct.put("Desc", desc);
        newProduct.put("Preço", preço);

        DocumentReference docRef = ff_db.collection(Global.COLLECTION_PRODUTOS).document(produtoId);
        docRef.set(newProduct).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dbh.setLocalProductUpdated(produtoId, imagem);
                ShowToast("Produtos atualizados");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                ShowToast("Erro ao atualizar produtos: " + e.toString());
            }
        });

        Log.d(TAG, "finishing updateFF\n");
        // EventListener for record events
    }

    private void uploadImage(final String produtoId, final String desc, final String tipo, final String preço, String imagem) {
        Log.d(TAG, "Start updateDB\n");

        //Uri filePath = Uri.parse(imagem);
        Uri filePath = Uri.fromFile(new File(imagem));
        Log.d(TAG, "uploadImage::Uri.parse(imagem): "+ filePath +"\n");

        if(filePath != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            StorageReference ref = storageReference.child(Global.COLLECTION_PRODUTOS + "/" + produtoId);

            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            ShowToast("Uploaded");

                            Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl()
                                    .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            imageURI = uri.toString();
                                            Log.d(TAG, "uploadImage:produtoId "+ produtoId +"\n");
                                            Log.d(TAG, "uploadImage:imageURI "+ imageURI +"\n");

                                            updateDB(produtoId, desc, tipo, preço, imageURI);
                                        }
                                    });

                        }
                    })

                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            ShowToast("Failed "+e.getMessage());
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded "+(int)progress+"%");
                        }
                    });
        }
        Log.d(TAG, "finishing uploadImage\n");

    }

    private void ShowToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
