package pt.jcampos.cartas.Menus;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pt.jcampos.cartas.Database.DatabaseHelper;
import pt.jcampos.cartas.R;
import pt.jcampos.cartas.Utils.Global;

public class MenuAdd extends AppCompatActivity {
    private static final String TAG = "MENU_ADD::::...";

    private DatabaseHelper dbh;
    private FirebaseFirestore db;

    private ListViewFragment produtPratoFragment;
    private ListViewFragment produtBebidaFragment;
    private ListViewFragment produtSobremesaFragment;

    private EditText edtMenuName;

    private String menuID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        setTheme(R.style.Theme_AppCompat);
        setTheme(R.style.Theme_AppCompat_Light);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_add);

        dbh = new DatabaseHelper(this);
        db = FirebaseFirestore.getInstance();

        ViewPager viewPager = (ViewPager) findViewById(R.id.vpProdutos);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tlProdutos);
        tabLayout.setupWithViewPager(viewPager);//setting tab over viewpager

        Button btnSave = (Button) findViewById(R.id.btnSaveNewMenu);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateMenu();
                // put the String to pass back into an Intent and close this activity
                Intent newIntent = new Intent();
                newIntent.putExtra("NewMenu", "");
                setResult(RESULT_OK, newIntent); // by definition Activity.RESULT_OK = 0
                finish();
            }
        });

        edtMenuName = (EditText) findViewById(R.id.edtMenuName);

        // Verifica se estamos a editar um produto
        menuID = getIntent().getStringExtra("MenuID");
        if (!"".equals(menuID)) {
            edtMenuName.setEnabled(false);
            loadMenu();
        }
    }

    private void loadMenu() {
        edtMenuName.setText(menuID);
        Log.d(TAG, "Start loadProduto\n");
        ArrayList<String> produtos = new ArrayList<>();

        String query = "SELECT m.Nome, p.Nome, p.Preço "
                    + "FROM MENU m, PRODUTO p, MENU_PRODUTO mp "
                    + "WHERE m.Nome = mp.MENU_Id "
                    + "AND p.Nome = mp.PRODUTO_Id "
                    + "AND m.Nome = '"+ menuID +"' "
                    + "ORDER BY mp.ID";

        SQLiteDatabase db = dbh.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String ProdutoID = cursor.getString(cursor.getColumnIndex("Nome"));
                String Preço = cursor.getString(cursor.getColumnIndex("Preço"));
                Log.d(TAG, "MENU_Id: " + menuID + ", PRODUTO_Id: " + ProdutoID + " => Preço: " + Preço + "\n");
                produtos.add(ProdutoID);

            } while (cursor.moveToNext());
            selectProductsFromMenu(produtos);

            Log.d(TAG, "finishing loadProduto\n");
        }
    }

    private void selectProductsFromMenu(ArrayList<String> produtos) {
        produtPratoFragment.setSelectedItem(produtos.get(0));
        produtBebidaFragment.setSelectedItem(produtos.get(1));
        produtSobremesaFragment.setSelectedItem(produtos.get(2));
    }

    private void updateMenu() {
        // Create a new user with a first and last name
        String name = edtMenuName.getText().toString();
        Map<String, Object> newProduct = new HashMap<>();
        newProduct.put("Nome", name);

        ArrayList<String> produtsList = new ArrayList<>();
        produtsList.add(produtPratoFragment.getSelectedItem());
        produtsList.add(produtBebidaFragment.getSelectedItem());
        produtsList.add(produtSobremesaFragment.getSelectedItem());

        newProduct.put("Produtos", produtsList);

        DocumentReference docRef = db.collection(Global.COLLECTION_MENUS).document(name);
        docRef.set(newProduct).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                ShowToast("Menu atualizado");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                ShowToast("Erro ao atualizar Menu: " + e.toString());
            }
        });
    }

    //Setting View Pager
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        produtPratoFragment = new ListViewFragment();
        produtPratoFragment.setTipo("Prato");
        adapter.addFrag(produtPratoFragment, "Prato");

        produtBebidaFragment = new ListViewFragment();
        produtBebidaFragment.setTipo("Bebida");
        adapter.addFrag(produtBebidaFragment, "Bebida");

        produtSobremesaFragment = new ListViewFragment();
        produtSobremesaFragment.setTipo("Sobremesa");
        adapter.addFrag(produtSobremesaFragment, "Sobremesa");

        viewPager.setAdapter(adapter);
    }

    private void ShowToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
