package pt.jcampos.cartas.Client;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import pt.jcampos.cartas.Database.DatabaseHelper;
import pt.jcampos.cartas.R;
import pt.jcampos.cartas.Utils.Global;

public class ReservaPedir extends Activity {
    private static final String TAG = "ReservaPedir::::... ";

    private DatabaseHelper dbh;

    private EditText edtClientReservaNumPessoas;
    private TextView tvClientReservaMenuData, tvClientPedidoReservaDetalhes;
    private String MenuID, Nome, Email;

    private Calendar date;
    private Button btnClientPedirReserva;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserva_pedir);

        dbh = new DatabaseHelper(this);

        edtClientReservaNumPessoas = (EditText) findViewById(R.id.edtClientReservaNumPessoas);
        edtClientReservaNumPessoas.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                updateReservaDetalhes();
            }
        });

        tvClientReservaMenuData = findViewById(R.id.tvClientReservaMenuData);

        tvClientPedidoReservaDetalhes = findViewById(R.id.tvClientPedidoReservaDetalhes);

        Button btnClientReservaMenuData = findViewById(R.id.btnClientReservaMenuData);
        btnClientReservaMenuData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDateTimeReserva();
            }
        });

        btnClientPedirReserva = (Button) findViewById(R.id.btnClientPedirReserva);
        btnClientPedirReserva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarPedidoReserva();
                finish();;
            }
        });

        MenuID = getIntent().getStringExtra("MenuID");
        TextView tvClientReservaMenu = (TextView) findViewById(R.id.tvClientReservaMenu);
        tvClientReservaMenu.setText(MenuID);
        updateReservaDetalhes();

        whoAmI();
    }

    private void updateReservaDetalhes() {
        tvClientPedidoReservaDetalhes.setText(MenuID + " para " + edtClientReservaNumPessoas.getText().toString()
        + " pessoas no dia " + tvClientReservaMenuData.getText().toString());
    }

    private void whoAmI() {
        Log.d(TAG,"whoAmI:");
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        Email = account.getEmail();
        Nome = account.getDisplayName();
    }

    private void guardarPedidoReserva() {
        if (tvClientReservaMenuData.getText().equals("") || edtClientReservaNumPessoas.getText().equals("")){
            ShowToast(Global.VERIFIQUE_DADOS_RESERVA);
            return;
        }
        String dataHora = tvClientReservaMenuData.getText().toString();
        String numPessoas = edtClientReservaNumPessoas.getText().toString();
        Log.d(TAG, "Reserva de " + MenuID + " para " + numPessoas + " em " + dataHora + " em nome de: " + Nome);

        dbh.insertPedidoReserva(Nome, Email, MenuID, dataHora, numPessoas);
    }

    /*

     */
    private void setDateTimeReserva() {
        final Calendar currentDate = Calendar.getInstance();
        date = Calendar.getInstance();
        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(ReservaPedir.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        date.set(Calendar.MINUTE, minute);
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        String dateTime = dateFormat.format(date.getTime());
                        tvClientReservaMenuData.setText(dateTime);
                        updateReservaDetalhes();
                        Log.d(TAG, String.format("The choosen one " + dateTime));
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), true).show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }

    private void ShowToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
