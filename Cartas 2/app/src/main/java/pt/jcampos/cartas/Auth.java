package pt.jcampos.cartas;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import pt.jcampos.cartas.Admin.MainActivity;
import pt.jcampos.cartas.Client.Client;
import pt.jcampos.cartas.Utils.Global;

public class Auth extends Activity {
    private static final String TAG = "Auth:::..  ";
    private static final int RC_SIGN_IN = 111;//google sign in request code
    private static GoogleSignInClient mGoogleSignInClient;//google sign in client

    private FirebaseFirestore ff_db;

    private ArrayList<String> admins;

    private TextView tvAuthStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate ");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        ff_db = FirebaseFirestore.getInstance();

        tvAuthStatus = (TextView) findViewById(R.id.tvAuthStatus);
        // Test Connectivity, if fails drop anything else
        if (!isNetworkOnline()){
            ShowToast(Global.NO_CONNECTIVITY);
            finish();
        }
        else{
            getAdminsFromFirebase();
        }
    }

    private void getAdminsFromFirebase() {
        Log.d(TAG, "getAdminsFromFirebase\n");
        admins = new ArrayList<>();

        CollectionReference produtos = ff_db.collection(Global.COLLECTION_ADMIN);

        Query query = produtos.orderBy("Nome");

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        admins.add(document.get("Email").toString());
                    }
                    for (String str: admins) {
                        Log.d(TAG, "Admin: "+ str + "\n");
                    }
                    configureGoogleSignIn();
                    checkGoogleSignIn();

                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart ");

        //doGoogleSignIn();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "Start onActivityResult: ");

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            //method to handle google sign in result
            handleSignInResult(task);
        }
    }

    /**
     * do google sign in
     */
    private void checkGoogleSignIn() {
        Log.d(TAG, "Start doGoogleSignIn: ");
        //get the last sign in account
        //if account doesn't exist do login else do sign out
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account == null) {
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);//pass the declared request code here
        }
        else
            checkProfileInformation(account);
    }


    /**
     * method to do google sign out
     * This code clears which account is connected to the app. To sign in again, the user must choose their account again.
     */
    public static void doGoogleSignOut(Activity activity) {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(activity , new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //Toast.makeText(MainActivity.this, "Google Sign Out done.", Toast.LENGTH_SHORT).show();
                        //revokeAccess();
                    }
                });
    }

    /**
     * method to handle google sign in result
     *
     * @param completedTask from google onActivityResult
     */
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        Log.d(TAG, "Start handleSignInResult: ");

        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            checkProfileInformation(account);

            //show toast
            //Toast.makeText(this, "Google Sign In Successful.", Toast.LENGTH_SHORT).show();

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.e(TAG, "signInResult:failed code= " + e.getStatusCode());

            //show toast
            Toast.makeText(this, "Failed to do Sign In: " + e.getStatusCode(), Toast.LENGTH_SHORT).show();

            //update Ui for this
            //getProfileInformation(null);
        }
    }

    /**
     * configure google sign in
     */
    private void configureGoogleSignIn() {
        Log.d(TAG, "Start configureGoogleSignIn: ");

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()//request email id
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void checkProfileInformation(GoogleSignInAccount account) {
        Log.d(TAG, "Start getProfileInformation: ");

        //Uri personPhoto = account.getPhotoUrl();

        Log.d(TAG,"\n personEmail: " + account.getEmail());
        Log.d(TAG,"\n personName: " + account.getDisplayName());
        Log.d(TAG,"\n personGivenName: " + account.getGivenName());
        Log.d(TAG,"\n personFamilyName: " + account.getFamilyName());
        Log.d(TAG,"\n personId: " + account.getId());

        if (admins.contains(account.getEmail())){
            Intent newIntent = new Intent(this, MainActivity.class);
            finish();
            startActivity(newIntent);
        }else{
            Intent newIntent = new Intent(this, Client.class);
            finish();
            startActivity(newIntent);
        }
    }

    public boolean isNetworkOnline() {
        boolean status=false;
        try{
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);
            if (netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED) {
                status= true;
            }else {
                netInfo = cm.getNetworkInfo(1);
                if(netInfo!=null && netInfo.getState()==NetworkInfo.State.CONNECTED)
                    status= true;
            }
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return status;
    }

    private void ShowToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
