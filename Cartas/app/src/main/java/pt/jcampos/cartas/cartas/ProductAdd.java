package pt.jcampos.cartas.cartas;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ProductAdd extends Activity {
    private static final String COLLECTION_PRODUTOS = "PRODUTOS";
    private static final String NOME_KEY = "Nome";
    private static final String DESC_KEY = "Desc";
    private static final String TIPO_KEY = "Tipo";
    private static final String PREÇO_KEY = "Preço";
    private static final String IMAGEM_KEY = "Imagem";

    private final int PICK_IMAGE_REQUEST = 71;

    private Uri filePath;

    // not really used so far
    private static final int RES_CODE_TEST = 2;

    private Button btnSave;
    private EditText edtNome, edtDesc, edtPreço;
    private RadioGroup rdgTipo;
    private ImageView imgvProduct;

    private String tipo = "";
    private String productId = "";
    private String imageURI = "";

    private FirebaseFirestore db;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private boolean newImageToUpload = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_add);

        db = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        edtNome = (EditText) findViewById(R.id.edtNome);
        edtDesc = (EditText) findViewById(R.id.edtDesc);
        edtPreço = (EditText) findViewById(R.id.edtPreço);

        // Verifica se estamos a editar um produto
        productId = getIntent().getStringExtra("ProductID");
        if (productId != "") loadProduct();

        rdgTipo = (RadioGroup) findViewById(R.id.rgTipo);
        rdgTipo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                tipo = rb.getText().toString();
                btnSave.setEnabled(true);
            }
        });

        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProduct();
                // put the String to pass back into an Intent and close this activity
                Intent newIntent = new Intent();
                newIntent.putExtra("NewProduct", "");
                setResult(RESULT_OK, newIntent); // by definition Activity.RESULT_OK = 0
                finish();
            }
        });

        imgvProduct = (ImageView) findViewById(R.id.imgvProduct);
        imgvProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null )
        {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imgvProduct.setImageBitmap(bitmap);
                //newImageToUpload = true;
                uploadImage();
            }
            catch (IOException e)
            {
//                e.printStackTrace();
                ShowToast(e.getMessage());
            }
        }
    }

    private void loadProduct() {
        CollectionReference produtos = db.collection(COLLECTION_PRODUTOS);

        Query query = produtos.whereEqualTo("Nome", productId);

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        imageURI = document.get("Imagem").toString();

                        edtNome.setText(document.getId());
                        edtDesc.setText(document.get("Desc").toString());
                        edtPreço.setText(document.get("Preço").toString());

                        String tipo = document.get("Tipo").toString();
                        if (tipo.equals("Prato")) rdgTipo.check(R.id.rbTipoPrato);
                        if (tipo.equals("Bebida")) rdgTipo.check(R.id.rbTipoBebida);
                        if (tipo.equals("Sobremesa")) rdgTipo.check(R.id.rbTipoSobremesa);
                    }

                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
                if (imageURI != "") loadImage(imageURI);
            }
        });
    }

    private void loadImage(String imageURI) {
        //Glide.with(this).using(new FirebaseImageLoader()).load(imageURI).into(imgvProduct);
        Glide.with(this).load(imageURI).into(imgvProduct);
    }

    /*
    *
    * */
    private void updateProduct() {
        // Create a new user with a first and last name
        Map<String, Object> newProduct = new HashMap<>();
        newProduct.put(TIPO_KEY, tipo);
        newProduct.put(IMAGEM_KEY, imageURI);
        newProduct.put(NOME_KEY, edtNome.getText().toString());
        newProduct.put(DESC_KEY, edtDesc.getText().toString());
        newProduct.put(PREÇO_KEY, edtPreço.getText().toString());

        DocumentReference docRef = db.collection(COLLECTION_PRODUTOS).document(edtNome.getText().toString());
        docRef.set(newProduct).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                ShowToast("Produtos atualizados");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                ShowToast("Erro ao atualizar produtos: " + e.toString());
            }
        });
    }

    /*
    *
    * */
    private void ShowToast(String message) {
        Toast.makeText(ProductAdd.this, message, Toast.LENGTH_SHORT).show();
    }

    private void uploadImage() {
        if(filePath != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            //StorageReference ref = storageReference.child("images/"+ UUID.randomUUID().toString());
            StorageReference ref = storageReference.child(COLLECTION_PRODUTOS + "/" + productId);

            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            ShowToast("Uploaded");

                            Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl()
                                    .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
//                                            newImageUploaded = true;
                                            imageURI = uri.toString();
                                        }
                                    });

                        }
                    })

                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            ShowToast("Failed "+e.getMessage());
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded "+(int)progress+"%");
                        }
                    });
        }
    }

}
