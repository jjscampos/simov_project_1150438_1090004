package pt.jcampos.cartas.cartas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class ProductList extends Activity {
    private static final String COLLECTION_PRODUTOS = "PRODUTOS";
    private static final int PRODUCT_ADD_REQUEST_CODE = 0;
    private static final int PRODUCT_UPDATE_REQUEST_CODE = 0;

    private Button btnAdd;
//    private FloatingActionButton fabAdd;
    private RadioGroup rdgTipo;

    private FirebaseFirestore db;

    private ListView lvProducts;
    private ProductViewAdapter productViewAdapter;
    ArrayList<Product> products;

    // TODO: 14/12/2018
    // Add a Floating Action Button for creating new product action

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO: 13/12/2018
        // pesquisar sobre melhor forma de aliviar o prcessamento neste metodo
        // talvez desviar para outro as instruções mais demoradas

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        // TODO: 13/12/2018
        // Seria muito proveitoso usar a referência ao base de forma global
        db = FirebaseFirestore.getInstance();

        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(ProductList.this, ProductAdd.class);
                newIntent.putExtra("ProductID", "NewProduct");

                startActivityForResult(newIntent, PRODUCT_ADD_REQUEST_CODE);
            }
        });

//        fabAdd = (FloatingActionButton) findViewById(R.id.fabAddProduct);
//        fabAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent newIntent = new Intent(ProductList.this, ProductAdd.class);
//                startActivityForResult(newIntent, PRODUCT_ADD_REQUEST_CODE);
//            }
//        });

        rdgTipo = (RadioGroup) findViewById(R.id.rgTipo);
        rdgTipo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);

                listFromQuery(rb.getText().toString());
            }
        });

        lvProducts = (ListView) findViewById(R.id.lvProducts);
        lvProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent newIntent = new Intent(ProductList.this, ProductAdd.class);
                Product product = (Product) productViewAdapter.getItem(position);
                newIntent.putExtra("ProductID", product.getId());
                startActivityForResult(newIntent, PRODUCT_UPDATE_REQUEST_CODE);
            }
        });
        listFromQuery("Prato");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) { // by definition Activity.RESULT_CANCELED = -1
//            return;
        }
        if (resultCode == RESULT_OK) { // by definition Activity.RESULT_OK = 0
            // get String data from Intent
            String returnString = data.getStringExtra("NewProduct");
            // update list info
            int selectedId = rdgTipo.getCheckedRadioButtonId();
            // find the radiobutton by returned id
            RadioButton rb = (RadioButton) findViewById(selectedId);
            listFromQuery(rb.getText().toString());
        }
    }

    private void listFromQuery(String tipo) {
        products = new ArrayList<Product>();

        CollectionReference produtos = db.collection(COLLECTION_PRODUTOS);
        // TODO: 13/12/2018
        // pesquisar como fazer: "firestore whereEqualTo wildcards",
        // para fazer semelhante ao SQL like %%
        //
        //Query query = produtos.whereEqualTo("Tipo", tipo);
        Query query = produtos.whereEqualTo("Tipo", tipo);

        /*
        TODO: 12/12/2018
        adicionar ordenação, por exemplo:
        .orderBy("Nome", Query.Direction.ASCENDING);
        */

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        products.add(new Product(document.getId(), document.get("Preço").toString(), document.get("Tipo").toString()));
                    }
                    productViewAdapter = new ProductViewAdapter(products);
                    lvProducts.setAdapter(productViewAdapter);

                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
            }
        });
    }

}

