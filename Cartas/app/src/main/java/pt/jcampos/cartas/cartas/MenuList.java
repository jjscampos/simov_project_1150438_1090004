package pt.jcampos.cartas.cartas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class MenuList extends Activity {
    private static final String TAG = "MenuList";
    private static final String COLLECTION_MENUS = "MENUS";
    private static final String COLLECTION_PRODUTOS = "PRODUTOS";
    private static final int MENU_ADD_REQUEST_CODE = 0;
    private static final int MENU_UPDATE_REQUEST_CODE = 0;

    private Button btnAdd;
    private FirebaseFirestore db;

    private ListView lvMenus;

    private MenuViewAdapter menuViewAdapter;
    private ArrayList<Menu> menus;
    private String finalPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_list);

        // TODO: 13/12/2018
        // Seria muito proveitoso usar a referência ao base de forma global
        db = FirebaseFirestore.getInstance();

        btnAdd = (Button) findViewById(R.id.btnAddMenu);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(MenuList.this, MenuAdd.class);
                newIntent.putExtra("MenuID", "NewMenu");

                startActivityForResult(newIntent, MENU_ADD_REQUEST_CODE);
            }
        });

        lvMenus = (ListView) findViewById(R.id.lvMenus);
        lvMenus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent newIntent = new Intent(MenuList.this, MenuAdd.class);
                Menu menu = (Menu) menuViewAdapter.getItem(position);
                newIntent.putExtra("MenuID", menu.getId());
                startActivityForResult(newIntent, MENU_UPDATE_REQUEST_CODE);
            }
        });

        listFromQuery("");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) { // by definition Activity.RESULT_CANCELED = -1
//            return;
        }
        if (resultCode == RESULT_OK) { // by definition Activity.RESULT_OK = 0
//            // get String data from Intent
//            // find the radiobutton by returned id
//            RadioButton rb = (RadioButton) findViewById(selectedId);
//            listFromQuery(rb.getText().toString());
//        }
        }
    }

    private void listFromQuery(String tipo) {
        menus = new ArrayList<Menu>();

        CollectionReference menusCR = db.collection(COLLECTION_MENUS);
        // TODO: 13/12/2018
        // pesquisar como fazer: "firestore whereEqualTo wildcards",
        // para fazer semelhante ao SQL like %%

        final Query query = menusCR.orderBy("Nome");

        /*
        TODO: 12/12/2018
        adicionar ordenação, por exemplo:
        .orderBy("Nome", Query.Direction.ASCENDING);
        */

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        // Produtos is a array of Strings
                        ArrayList<String> produtos = (ArrayList<String>)document.get("Produtos");
                        CalculateTotalPrice(document.getId(), produtos);
                        //menus.add(new Menu(document.getId(), produtos, ""));
                    }
//                    menuViewAdapter = new MenuViewAdapter(menus);
//                    lvMenus.setAdapter(menuViewAdapter);

                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
            }
        });
    }

    private void CalculateTotalPrice(String menuId, final ArrayList<String> produtos) {
        menus = new ArrayList<Menu>();

        db = FirebaseFirestore.getInstance();

        CollectionReference produtosCR = db.collection(COLLECTION_PRODUTOS);

        // foreach item of the arraylist
        //for (String prd : produtos) { /*query*/ }
        //citiesRef.where("state == CA || state == AZ")
        Query query = produtosCR.whereEqualTo("Nome", produtos.get(0));
        //Query query = produtosCR.whereEqualTo("Nome", produtos.get(0)+ " || " + produtos.get(1) + " || " + produtos.get(2));


        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            String price;
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        finalPrice = document.get("Preço").toString();
                        menus.add(new Menu(document.getId(), produtos, finalPrice));
                    }
                    menuViewAdapter = new MenuViewAdapter(menus);
                    lvMenus.setAdapter(menuViewAdapter);
                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
            }
        });

    }

}
