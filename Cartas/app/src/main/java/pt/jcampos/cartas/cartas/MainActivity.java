package pt.jcampos.cartas.cartas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class MainActivity extends Activity {
    private static final String COLLECTION_PRODUTOS = "PRODUTOS";
    // Add a different request code for every activity you are starting from here
    private static final int REQUEST_CODE_PRODUCT_ADD = 1;
    private static final int REQUEST_CODE_MENU_ADD = 2;
    private static final int REQUEST_CODE_CARTA_ADD = 3;

    private Button btnProducts, btnMenus, btnCartas;
    private TextView txtList;

    private FirebaseFirestore db;

    /*
    TODO: 12/12/2018
    adicionar restantes metodos override onStar, onStop, onResmo.
    Estudar o impacto e vantagens na sua utilização! :-)
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = FirebaseFirestore.getInstance();

        txtList = (TextView) findViewById(R.id.txtList);

        btnProducts = (Button) findViewById(R.id.btnProducts);
        btnProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                TODO: 12/12/2018
                Verificar o requestcode em função de atuar no resumo de existencias neste layout
                */
                Intent newIntent = new Intent(MainActivity.this, ProductList.class);
                startActivityForResult(newIntent, REQUEST_CODE_PRODUCT_ADD);
            }
        });

        btnMenus = (Button) findViewById(R.id.btnMenus);
        btnMenus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(MainActivity.this, MenuList.class);
                startActivityForResult(newIntent, REQUEST_CODE_MENU_ADD);
            }
        });

        btnCartas = (Button) findViewById(R.id.btnCartas);
        btnCartas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_CANCELED) { // by definition Activity.RESULT_CANCELED = -1
            txtList.append("onActivityResult: RESULT_CANCELED = " + RESULT_CANCELED);
//            return;
        }
        if (resultCode == RESULT_OK) { // by definition Activity.RESULT_OK = 0
            // get String data from Intent
            String returnString = data.getStringExtra("NewProduct");
            txtList.append("onActivityResult: RESULT_OK =" + RESULT_OK);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void listRecords() {
        db.collection(COLLECTION_PRODUTOS).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                txtList.append("Document: " + document.getId() + " => " + document.getData() + "\n\n\n");
                            }
                        } else {
                            txtList.append("Error getting documents: " + task.getException());
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        txtList.append("Error updating document: " + e.toString());
                    }
                });
    }
}
