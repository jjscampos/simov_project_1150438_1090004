package pt.jcampos.cartas.cartas;

public class Product {
    private String id, preço, tipo;

    public String getId() {
        return id;
    }

    public String getPreço() {
        return preço;
    }

    public String getTipo() {
        return tipo;
    }

    public Product (String id, String preço, String tipo)
    {
        this.id = id;
        this.preço = preço;
        this.tipo = tipo;
    }
}
