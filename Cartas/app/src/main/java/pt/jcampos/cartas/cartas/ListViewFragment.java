package pt.jcampos.cartas.cartas;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class ListViewFragment extends Fragment {
    private static final String COLLECTION_PRODUTOS = "PRODUTOS";

    private FirebaseFirestore db;

    private Context context;

    public ListViewAdapter adapter;
    private ArrayList<String> arrayList;
    private String tipo;
    private String selectedItem = "";

    public ListViewFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_view_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listFromQuery(view);

    }

    /*
    *
    */
    private void listFromQuery(final View view) {
        final ListView listView = (ListView) view.findViewById(R.id.list_view);
        arrayList = new ArrayList<>();

        db = FirebaseFirestore.getInstance();

        CollectionReference produtos = db.collection(COLLECTION_PRODUTOS);

        Query query = produtos.whereEqualTo("Tipo", tipo);

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        arrayList.add(document.getId());
                    }

                    adapter = new ListViewAdapter(context, arrayList);
                    adapter.setItemChecked(new ListViewAdapter.ItemChecked() {
                        @Override
                        public void onItemChecked(String name) {
                            selectedItem = name;
                        }
                    });
                    listView.setAdapter(adapter);

                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
            }
        });
    }

    /*
     *
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /*
    *
    */
    public String getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(String produto) {
        selectedItem = produto;
    }
}
