package pt.jcampos.cartas.cartas;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import io.opencensus.internal.StringUtil;

public class ListViewAdapter extends BaseAdapter {
    private static final String TAG = "ListViewAdapter";

    private Context context;
    private ArrayList<String> arrayList;
    private LayoutInflater inflater;
    private int selectedPosition = -1;

    public ListViewAdapter(Context context, ArrayList<String> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            viewHolder = new ViewHolder();

            //inflate the layout on basis of boolean
            view = inflater.inflate(R.layout.list_view_custom_menus, viewGroup, false);

            viewHolder.label = (TextView) view.findViewById(R.id.lbProductName);
            viewHolder.radioButton = (RadioButton) view.findViewById(R.id.rbProductSelected);

            view.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) view.getTag();

        viewHolder.label.setText(arrayList.get(i));

        //check the radio button if both position and selectedPosition matches
        viewHolder.radioButton.setChecked(i == selectedPosition);

        //Set the position tag to both radio button and label
        viewHolder.radioButton.setTag(i);
        viewHolder.label.setTag(i);

        //Toast.makeText(view.getContext(), "viewHolder.label(" + i + "): " + viewHolder.label.getText().toString(), Toast.LENGTH_SHORT).show();
        String message = "viewHolder.label(" + i + "): " + ", selectedPosition[" + selectedPosition + "] => "  + viewHolder.label.getText().toString();
        Log.d(TAG, message);

        viewHolder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemCheckChanged(v);
            }
        });

        viewHolder.label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemCheckChanged(v);
            }
        });
        return view;
    }

    //On selecting any view set the current position to selectedPositon and notify adapter
    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
        if (listener != null) listener.onItemChecked(arrayList.get(selectedPosition));
    }

    // TODO: 16/12/2018
    // simplify this one, perhaps you do not need this...
    private class ViewHolder {
        private TextView label;
        private RadioButton radioButton;
    }

    //
    public interface ItemChecked { void onItemChecked (String name); }
    private ItemChecked listener;

    public void setItemChecked (ItemChecked listener) {
        this.listener = listener;
    }
}

