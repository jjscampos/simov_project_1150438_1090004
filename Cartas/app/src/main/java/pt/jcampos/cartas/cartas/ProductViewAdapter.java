package pt.jcampos.cartas.cartas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class ProductViewAdapter extends BaseAdapter {
    private final List<Product> items;

    public ProductViewAdapter(final List<Product> items) {
        this.items = items;
    }

    public int getCount() {
        return this.items.size();
    }

    public Object getItem(int arg0) {
        return this.items.get(arg0);
    }

    public long getItemId(int arg0) {
        return arg0;
    }

    public View getView(int arg0, View arg1, ViewGroup arg2) {
        // TODO Auto-generated method stub
        final Product row = this.items.get(arg0);
        View itemView = null;

        if (arg1 == null) {
            LayoutInflater inflater = (LayoutInflater) arg2.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.product_item, null);
        } else {
            itemView = arg1;
        }

        // Set the text of the row
        TextView txtProductID = (TextView) itemView.findViewById(R.id.txtProductID);
        txtProductID.setText(row.getId());

//        TextView txtProductTipo = (TextView) itemView.findViewById(R.id.txtProductTipo);
//        txtProductTipo.setText(row.getTipo());

        TextView txtProductPreço = (TextView) itemView.findViewById(R.id.txtProductPreço);
        txtProductPreço.setText(row.getPreço());

        return itemView;
    }
}
