package pt.jcampos.cartas.cartas;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MenuAdd extends AppCompatActivity {
    private static final String COLLECTION_MENUS = "MENUS";

    private FirebaseFirestore db;

    private ListViewFragment produtPratoFragment;
    private ListViewFragment produtBebidaFragment;
    private ListViewFragment produtSobremesaFragment;

    private EditText edtMenuName;

    private String menuID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        setTheme(R.style.Theme_AppCompat);
        setTheme(R.style.Theme_AppCompat_Light);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_add);

        db = FirebaseFirestore.getInstance();

        ViewPager viewPager = (ViewPager) findViewById(R.id.vpProdutos);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tlProdutos);
        tabLayout.setupWithViewPager(viewPager);//setting tab over viewpager

        Button btnSave = (Button) findViewById(R.id.btnSaveNewMenu);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveNewMenu();
            }
        });

        edtMenuName = (EditText) findViewById(R.id.edtMenuName);

        // Verifica se estamos a editar um produto
        menuID = getIntent().getStringExtra("MenuID");
        if (menuID != "") loadMenu();

    }

    private void loadMenu() {
        edtMenuName.setText(menuID);

        CollectionReference menusQR = db.collection(COLLECTION_MENUS);

        Query query = menusQR.whereEqualTo("Nome", menuID);

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        ArrayList<String> produtos = (ArrayList<String>)document.get("Produtos");
                        ShowToast("Produtos Array: " + produtos.toString());
                        selectProductsFromMenu(produtos);
                    }

                } else {
                    //txtListAll.append("NOT POSSIBLE!!!!isSuccessful");
                }
            }
        });
    }

    private void selectProductsFromMenu(ArrayList<String> produtos) {
        produtPratoFragment.setSelectedItem(produtos.get(0));
        produtBebidaFragment.setSelectedItem(produtos.get(0));
        produtSobremesaFragment.setSelectedItem(produtos.get(0));

    }

    private void SaveNewMenu() {
        ShowToast("Prato: " + produtPratoFragment.getSelectedItem() + "\n"
                + "Bebida: " + produtBebidaFragment.getSelectedItem() + "\n"
                + "Sobremesa: " + produtSobremesaFragment.getSelectedItem());
        updateMenu();
    }

    private void updateMenu() {
        // Create a new user with a first and last name
        String name = edtMenuName.getText().toString();
        Map<String, Object> newProduct = new HashMap<>();
        newProduct.put("Nome", name);

        ArrayList<String> produtsList = new ArrayList<>();
        produtsList.add(produtPratoFragment.getSelectedItem());
        produtsList.add(produtBebidaFragment.getSelectedItem());
        produtsList.add(produtSobremesaFragment.getSelectedItem());

        newProduct.put("Produtos", produtsList);

        DocumentReference docRef = db.collection(COLLECTION_MENUS).document(name);
        docRef.set(newProduct).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                ShowToast("Menu atualizado");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                ShowToast("Erro ao atualizar Menu: " + e.toString());
            }
        });
    }

    //Setting View Pager
    private void setupViewPager(ViewPager viewPager) {
        //FragmentActivity
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        produtPratoFragment = new ListViewFragment();
        produtPratoFragment.setTipo("Prato");
        adapter.addFrag(produtPratoFragment, "Prato");

        produtBebidaFragment = new ListViewFragment();
        produtBebidaFragment.setTipo("Bebida");
        adapter.addFrag(produtBebidaFragment, "Bebida");

        produtSobremesaFragment = new ListViewFragment();
        produtSobremesaFragment.setTipo("Sobremesa");
        adapter.addFrag(produtSobremesaFragment, "Sobremesa");

//        adapter.addFrag(new ListViewFragment(), "Prato");
        viewPager.setAdapter(adapter);
    }

    private void ShowToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
